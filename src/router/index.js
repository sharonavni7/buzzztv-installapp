import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/error',
    name: 'error',
    component: () => import('../views/ErrorPage.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginPage.vue')
  },
  {
    path: '/filter_page',
    name: 'filter_page',
    component: () => import('../views/FilterPage.vue')
  },
  {
    path: '/select_sensor',
    name: 'select_sensor',
    component: () => import('../views/SelectSensor.vue')
  },
  {
    path: '/sensor_add_page',
    name: 'sensor_add_page',
    component: () => import('../views/SensorAddPage.vue')
  },
  {
    path: '/assign_success',
    name: 'assign_success',
    component: () => import('../views/AssignedSuccessfuly.vue')
  },
  {
    path: '/qr_scanner',
    name: 'qr_scanner',
    component: () => import('../views/QrScanner.vue')
  },

]

const router = new VueRouter({
  routes
})


router.beforeEach(async (to, from, next) => {
  if(to.name === 'login' || to.name === 'error') return next()
  let user;
  user = await store.dispatch('getLoggedInUser');
  if(!user) return next('/login')
  if(to.path === '/') return next('/filter_page')

  return next()
})

export default router