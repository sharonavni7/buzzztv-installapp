export function roomName(room) {
    let name = "";
    name += room.site_name ? room.site_name + " / " : "";
    name += room.building ? room.building + " / " : "";
    name += room.floor ? room.floor + " / " : "";
    name += room.zone ? room.zone + " / " : "";
    name += room.gender ? room.gender + " / " : "";
    name += room.display_name ? room.display_name : "";

    return name;
}

export default { roomName };
