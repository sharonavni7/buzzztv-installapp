
const state = {
    table: [],
    selection_dict: [],
    anti_dict: [],
    storage_db: {}
};

const getters = {
    gettable: (state) => state.table,
    getFilteredItems(state) {
        let resultData = state.table

        for (const entry in state.selection_dict) {
            const field=state.selection_dict[entry][0];
            const value=state.selection_dict[entry][1];


            resultData = resultData.filter(item => item[field] === value)
        }
        for (const entry in state.anti_dict) {
            const field=state.anti_dict[entry][0];
            const value=state.anti_dict[entry][1];

            resultData = resultData.filter(item => item[field] !== value)
        }

        return resultData
    },
    getFromStorage: (state) => (name) => state.storage_db[name]
}

const actions = {
    fetchData({ commit }, table) {
        commit("setInitialData", table);
    },
    updateFieldFilter({ commit }, payload) {
        commit("changeFieldValue", payload)
    },
    updateNotFieldFilter({ commit }, payload) {
        commit("changeAntiFieldValue", payload)
    },
    resetSelections({ commit }) {
        commit("clearSelections")
    },
    saveToStorage({commit}, payload){
        commit("assignToStorage", payload)
    }
};

const mutations = {
    setInitialData(state, table) {
        state.table = table;
        state.selection_dict = []
        state.anti_dict = []
    },
    changeFieldValue(state, payload) {
        const field = payload[0]
        const new_val = payload[1]

        state.selection_dict.push([field, new_val]);
        state.table = [...state.table]
    },
    changeAntiFieldValue(state, payload) {
        const field = payload[0]
        const new_val = payload[1]

        state.anti_dict.push([field, new_val]);
        state.table = [...state.table]
    },
    assignToStorage(state, payload) {
        const name = payload[0]
        const new_val = payload[1]

        state.storage_db[name] = new_val;
    },
    clearSelections(state) {
        state.selection_dict = []
        state.anti_dict = []
    }

};

export default { state, getters, actions, mutations };
