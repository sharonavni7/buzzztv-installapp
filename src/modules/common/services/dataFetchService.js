import ajax from '../../common/services/httpService';


async function getSiteRoomsTable() {
    let table = await ajax.get("room/siteRooms");

    for (const index in table) {
        table[index]['site_name'] = table[index]["city_name"] + "/" + table[index]["company_name"];
    }

    return table;
}


async function getSensorsForRoom(roomId) {
    const query = { room_id: roomId };
    return await ajax.get("sensor", null, query);
}


async function updateSensor(device) {
    return await ajax.put("sensor", device);
}

export default {
    getSiteRoomsTable,
    getSensorsForRoom,
    updateSensor
}