import router from '@/router'

const BASE_URL = 'http://51.77.112.79:3000/api/'
//const BASE_URL = 'https://portal.buzzztech.com/api/'
//const BASE_URL = 'http://localhost:3000/api/'
//const BASE_URL = '/api/'


// import Axios from 'axios';
// var axios = Axios.create({
//     withCredentials: true
// });

import axios from 'axios'
axios.defaults.withCredentials = true;

async function ajax(endpoint, method='get', data=null, query=null) {   
    try {
        const res = await axios({
            url: BASE_URL + endpoint,
            method,
            data,
            params:query
        })
        return res.data;
    } catch (err) {
        // eslint-disable-next-line no-console
        console.log(err);
        
        if (err.response.status === 401) {
            router.push('/error');
        } 
        throw err.response.data.error
    }
}

export default {
    get(endpoint, data, query){
        return ajax(endpoint, 'GET', data, query)
    },
    post(endpoint, data){
        return ajax(endpoint, 'POST', data)
    },
    put(endpoint, data){
        return ajax(endpoint, 'PUT', data)
    },
    delete(endpoint, data){
        return ajax(endpoint, 'DELETE', data)
    }

}