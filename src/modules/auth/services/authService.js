import ajax from '../../common/services/httpService';
const BASE_URL = 'auth'

/**
 * @param {Object} userCred
 * @param {string} userCred.email
 * @param {string} userCred.password
 */
async function login(userCred) {
    try {
        const user = await ajax.post(`${BASE_URL}/login`, userCred)
        return user
    } catch (err) {
        if (err && err.status === 401) {
            throw new Error('Wrong email or password')
        } else {
            throw new Error(err);
        }
    }
}

async function getLoggedInUser() {
    try {
        const user = await ajax.get(`${BASE_URL}/loggedInUser`);
        return user
    } catch(err) {
        throw err;
    }
}

async function logout() {
    ajax.post(`${BASE_URL}/logout`)
    sessionStorage.clear();
}



export default {
    login,
    getLoggedInUser,
    logout
}

