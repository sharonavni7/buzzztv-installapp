import authService from '../services/authService'
export default {
    state: {
        loggedUser: null,
        authError: ''
    },
    getters: {
        loggedUser(state) {
            return state.loggedUser;
        },
        authErrorMsg(state) {
            return state.authError
        }
    },
    actions: {
        async login(context, { credentials }) {
            try {
                const user = await authService.login(credentials)
                context.commit({ type: 'setAuthError', error: '' })
                context.commit({ type: 'setLoggedUser', user })
                return user
            } catch (err) {
                context.commit({ type: 'setAuthError', error: err.message })
            }
        },
        async getLoggedInUser(context) {
            try {
                const user = await authService.getLoggedInUser();
                context.commit({ type: 'setLoggedUser', user });
                return user;
            } catch (err) {
                //handle UI error
            }
        },
        async logout(context) {
            try {
                await authService.logout();

            } catch (err) {
                //handle UI error
            }
            context.commit('setLogout');
        }
    },
    mutations: {
        setLoggedUser(state, { user }) {
            state.loggedUser = user;
        },
        setAuthError(state, { error }) {
            state.authError = error;
        },
        setLogout(state) {
            state.loggedUser = null;
            state.authError = '';

        }
    },

}