import Vue from 'vue';
import Vuex from 'vuex';
import dbtable from '../modules/common/store/dbtable'
import authStore from '../modules/auth/store/authStore'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    dbtable,
    authStore
  },
  plugins: [
    createPersistedState()
  ]
})

