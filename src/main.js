import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'

import Vuelidate from 'vuelidate';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPaperclip, faQrcode, faUser, faMapMarkerAlt, faHome, faKeyboard, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPaperclip)
library.add(faQrcode)
library.add(faUser)
library.add(faMapMarkerAlt)
library.add(faHome)
library.add(faKeyboard)
library.add(faSignOutAlt)

Vue.use(Vuelidate);

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

